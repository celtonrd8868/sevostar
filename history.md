## 소개
안녕하세요?
이제 막 3년 5개월차에 접어든 개발자 입니다.<br>
비슷한 연차에 개발자들과 마찬가지로, 지난 시간동안 하고 싶은 분야가 무엇인지 많이 고민해왔습니다.<br>
다행히 관심갖고 있던 웹 관련 업무를 스스로 지원해서 맡게 되면서 집중하고 싶은 마음이 생겼고,<br>
앞으로는 마음 맞는 분들과 함께 좋은 팀을 만들어가며 서비스개발에 주도적으로 참여하고 싶습니다.<br>

***

## 기술스택
* 새로운 기술을 받아들이고 적용하는 것에 대해 유연한 마인드를 소유하고 있습니다.
* 관심이 있는 기술을 계속해서 학습하고 기회가 생길때 바로 적용할수 있도록 준비하는 편이며, 빠르게 학습하여 적용 할 수 있습니다.
* 개발 중 발생한 문제는 Stack Overflow, Gihub Issue, 공식 문서와 구글링 등을 통해 해결해오고 있고 영문 기술 문서를 읽는데 지장이 없습니다.
* 아래 내용은 현재 사용중인 기술과 관심사에 대해 웹 관련 내용을 중심으로 가감없이 정리해본 내용입니다. 개인적인 성격/성향 등은 면접시 판단해 주시면 좋을것 같습니다.

#### Node.js (with Typescript)
* 백앤드는 Express 프레임워크를 사용하고 있습니다.
* 실시간 데이터를 다루기 위해 MQTT 프로토콜과 WebSocket을 사용하고 있습니다.
* 실시간 데이터를 제외하고, Rest API 형태로 작성하였고 JWT를 통해 인증 기능을 구현하였습니다.
* 데이터베이스는 MySQL을 사용중이며, TypeORM을 사용하고 있습니다.
* IOC컨테이너를 사용하기 위해 Inversify를 사용하고 있으며, OOP환경의 프로그래밍 경험을 적극 활용하고 있습니다.

#### React.js (with Typescirpt)
* 프론트앤드는 React.js를 사용하여 SPA을 개발하고 있습니다. (※ 프론트앤드는 Angular 2+를 통해 입문하였습니다.)
* MobX를 통해 상태관리를 하고 있으며, Websocket을 통해 전달되는 데이터를 시각화하는 기능을 구현하였습니다.
* 반응형 UI(데스크탑/모바일)를 구현하였고, 디자이너가 없는 환경에서 작업하여 UI 컴포넌트를 활용하였습니다.

#### 협업도구
* 협업 도구는 Bitbucket/Gitlab, Slack, Redmine, Trello 정도를 사용해 보았으나, 추후 어떤 도구를 사용하더라도 적극 활용 가능합니다.

#### Python
* gRPC를 이용한 API 개발을 통해 업무중 발생한 작은 문제들을 해결하는 도구로 활용중 입니다.
* Python만을 이용한 프로젝트 경험이 없어 자유롭게 Pythnic한 코드를 만들지 못하지만, 모든 분야에서 매우 유용한 도구로 활용하고 있고, 앞으로도 적극 활용할 생각입니다.

#### Linux 활용
* 대학 4년간 리눅스 동아리 활동과 전산실 아르바이트를 하면서 리눅스 환경에 익숙한편입니다.
* 현재 진행 중인 프로젝트는 내부망을 사용하기 때문에, 가상환경의 리눅스 서버에 자체 서버를 구축하여 Nginx와 PM2를 통해 운용하고 있습니다.

#### 관심 & 학습중인 기술
* Serverless 클라우드 기술
  * 모두 나열 할 수 없지만 기회가 생길때마타 틈틈히, 세미나/강좌등에 참여하면서 AWS나 AZURE같은 클라우드 기술에 계속 관심을 갖고 있습니다.
  * 추후 클라우드 기술을 사용하게 된다면 빠르게 적응하여 사용할수 있습니다.
  * 최근 토이프로젝트에 적용하기 위해 Serverless 기술에 관심을 갖고 틈틈히 학습중입니다.
* Javascirpt
  * 그동안 OOP 개발 환경에 익숙하였지만 Javascript를 접하면서 최근 함수형 프로그래밍에 관심을 갖게 되었으며, RxJS와 함께 학습하고 있습니다.
* 단위 테스트
  * 그동안 완성된 S/W에 대한 양산 단계의 테스트를 고려하여 개발하였지만, 웹 개발 환경 특성상 단위 테스트가 중요하다는 인식을 갖게 된 후 단위 테스트에 관심을 갖게 되었습니다.

***
# 경력 기술

> - 최근 진행한 프로젝트 순으로 정리하였습니다.<br>
> - 웹 분야가 아닌 프로젝트도, UI가 있는 프로젝트 위주로 정리 하였으니 참고용으로만 보아주시기 바랍니다.<br>
> - 일부 사이즈가 큰 이미지가 있어 로딩에 시간이 소요될 수 있습니다.<br>

***
## (주) SENTROL
* 재직기간 : 2016.12 ~ 현재
* 업무분야 : 3D프린터, 스마트 팩토리 관련 응용 S/W 개발

### 유지보수
* SS150, SS600G 주물사 3D 프린터 프린팅 관리 S/W 유지보수 (C#, 윈폼)
* 오픈소스 슬라이싱 프
* 로그램인 Slic3r 커스터마이징 및 유지보수 (C++)

### CSL NExt(Nextwork extension)
* 개발기간
  * 2017.10 ~ 현재
* 주요기능
  * 스마트펙토리 솔루션 개발을 위한 선행개발 프로젝트
  * 로컬 환경의 Linux기반 CNC Controller를 인터넷 상에서 모니터링/제어
  * 복수 장비를 모니터링 하고 관리하는 SPA앱 개발
  * 장비와 센서에서 나오는 실시간 데이터 처리/시각화
  * REST API 개발
* 기술 스텍
  * 백앤드 : 
    * 언어/프레임워크 : Typescript / Node.js(Express)
    * Mqtt Protocol(C++), Websocket(Socket.IO)
    * Rest API 구축 / JWT 인증
    * MySQL, TypeORM
    * Inversify(IOC 컨테이너)
    * Nginx, PM2
  * 프론트앤드 
    * 언어/프레임워크 : Typescript / React
    * 상태관리 : MobX 
    * Blueprint.js / Styled-Component, CSS (※ Flex, Grid Layout)
    * Desktop 및 Mobile 반응형 UI
  * 기타
     * 배포 OS : Ubuntu16.04 Hyper-V 가상 환경
     * 기타 도구 : Gitlab, Slack, Redmine
  * 동작화면 (※ 개발중 캡쳐한 이미지로 자체 운용버전과 다소 차이가 있습니다.)

  <center>
    <img
      src="./CSLNExt/CSLNextDesktop.gif"
      width="580"
      height="340px"
      title="CSLNext" />
    <p>Desktop 화면</p>
  </center>
  
  <center>
    <img
      src="./CSLNExt/CSLNextMobile.gif"
      width="260"
      height="425px"
      title="CSLNext" />
    <p>Mobile 화면</p>
  </center>

***

### BinderJet 프린팅 S/W
* 개발기간
  * 2017.05 ~ 2017.11

* 주요기능
  * Binderjet방식의 3D프린터 SB300, SB400의 바인더 잉크헤드, 축 제어 및 모니터링
  * 3D파일로부터 슬라이싱된 대량의 이미지를 바인더 잉크헤드용 규격으로 가공
  * 출력 모니터링, 바인더 분사량 조절 등 기능 수행
  * 슬라이싱 이미지 가공 및 헤더제어 부분은 재사용 가능한 모듈로 개발 되어 SB1000모델에 적용

* 기술 스텍
  * GUI S/W : C# WPF, Async Socket, 바인더 잉크헤드 제어 등

* 결과물
  
  <center>
    <img
      src="./Binderjet/Binderjet.gif"
      width="540"
      height="320px"
      title="Binderjet" />
    <p>Binderjet 테스트</p>
    <p>※ 개발 중 테스트 목적으로 촬영한 이미지로 완성된 결과물과는 차이가 있음</p>
  </center>

***

### Materialize Data Viewer
* 개발기간
  * 2017.02 ~ 2017.05
* 주요기능
  * SM250 금속 3D프린터의, 메인 프린팅 관리 S/W에서 실행되는 보조프로그램
  * Materialize사의 제어기를 장착한 3D프린터 버전에 탑재
  * Materialize사의 전용 Binary 3D포맷의 파싱 및 형상 관리
  * 주요 기능은 재사용 가능한 모듈로 개발
* 기술 스텍
  * GUI S/W : C# WPF, OpenGL 등
* 결과물

  <center >
    <img
      src="./DataViewer/DataViewer.gif"
      width="540
      height="320px"
      title="Data Viewer">
    <p>Materialize Data Viewer</p>
  </center>
  
***

### G-Code Viewer
* 개발기간

  * 2016.12 ~ 2017.2
* 주요기능
  * CNC컨트롤러 제어를 위한 G-Code를 형상화(2D)하는 프로그램
  * CS팀 요청으로 고객 임의로 작성하거나 수정한 G-Code의 기술지원을 위해 제작
  * 자사에 특화된 G-Code 예약어, 커스텀 기능 지원
    
* 역할 및 업무
  * 자사 스펙의 G-Code 파싱 및 2D 출력 S/W 개발 
* 기술스텍
  * GUI S/W : C# 윈폼, WPF Canvas 등
* 결과물
  
  <center >
    <img
      src="./GCodeViewer/GCodeViewer.gif"
      width="540
      height="320px"
      title="G-Code Viewer">
    <p>G-Code Viewer</p>
  </center>

***

## (주) PLATO 
* 재직기간 : 2015.08 ~ 2016.12
* 업무분야 : 자동차 전장 S/W 개발

### 유지보수
* 자체 개발 차량 블랙박스(옵니와칭) 영상 모니터링 S/W 버그픽스 및 유지보수 업무 (C++/MFC)

### Display FOB
* 개발 기간
  * 2016.05 ~ 2016.12
* 주요 기능
  * 디스플레이를 갖춘 차량 스마트키 선행 개발 프로젝트로, 양산 중인 H사의 스마트키 기반으로, 제공된 대형 차량 데이터 이용
* 역할 및 업무
  * GUI 개발을 담당하는 외부 개발사 및 자사 H/W 팀과 협업
  * Display 스마트키 벡엔드 기능 전체 개발 (LF인증, RF통신, I2C통신, 배터리 모니터링 등)
* 기술 스택
  * FOB : C언어 (MSP430 MCU, NCK2983 RF Tranceiver, MRF2678 LF Receiver)
  * 모니터링 S/W : C# WPF 등
* 결과물
  * Display FOB의 (GUI 제외)전체 기능
  * FOB 내부 기능 설정 및 로그 모니터링 S/W, 차량측 시뮬레이션 S/W
  
  <center>
    <img
      src="./DisplayFOB/DisplayFOBAction.gif"
      width="220px"
      height="360px"
      title="Basestation">
    <p>Display Fob 동작</p>
  </center>

***

### Low Frequency Basestation
* 개발 기간
  * 2015.12 ~ 2016.06
* 주요기능 
  * 차량 스마트키 3대의 LF(Low Frequency) RSSI 신호를, RF(Radio Frequency)신호로 수집 및 1차 가공
  * 위치 계산을 위한 데이터 프레임 생성 및 출력하는 장치
* 역할 및 업무
  * Basestation 제어 S/W 및 모니터링, 플래쉬 메모리 라이팅 S/W 개발
  * GUI 개발을 담당하는 중국 업체 및 H/W개발을 담당하는 국내 업체 중국 법인과 협업
* 기술스택
  * Basestation : C언어, STM8 MCU, TRF 4140/4260 LF Tranceiver, ATAs5781 RF
  * 모니터링 개발 : 모니터링/분석 툴 : C# WPF, PyQT
* 결과물
  * Basestation 제어 S/W 개발
  * 로그 및 데이터 모니터링 S/W 개발
  * 플래쉬 메모리 라이팅 S/W 개발

***